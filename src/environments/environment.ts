// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  version: require('../../package.json').version,
  apiBaseURL: 'http://localhost:3001/',
  stripeKey: 'pk_test_XcOzizDPEjbKrsAudnvkNJly',
  shippoKey: 'shippo_test_1b0bb31d80d9a20971b8b59150e8ca117ffff732'
};
