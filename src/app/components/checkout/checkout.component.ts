import { Component, OnInit, ViewChild, AfterViewInit, Output, EventEmitter, OnDestroy} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderService } from '../../services/order.service';
import { OrderType } from '../../interfaces/order';
import { UsStatesService } from '../../services/us-states.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit, OnDestroy {

  checkoutForm: FormGroup;
  order: OrderType;
  thereArePhysicalProducts: boolean;
  states: any;

  @Output() canGoNext: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private orderService: OrderService,
    private usStatesService: UsStatesService
  ) { }

  ngOnInit() {
    this.states = this.usStatesService.getStates();
    this.orderService.order.subscribe( order => this.useOrder(order))
  }

  ngOnDestroy(){
    this.orderService.emitNewState(this.order);
  }

  useOrder(order: OrderType){
    this.order = order;
    this.createCheckoutForm();
    this.thereArePhysicalProducts = this.orderService.thereArePhysicalProducts(order);
  }

  createCheckoutForm(){
    this.checkoutForm = this.formBuilder.group({
      firstName: ['', Validators.required ],
      lastName: ['', Validators.required ],
      email: ['', [Validators.required, Validators.email] ],
      sendMarketing: [true],
      schoolOrOrg: [false],
      accountName: [''],
      accountNumber: [''],
      schoolOrEnsemble: [''],
      conductorsName: [''],
      billingAddress: this.formBuilder.group({
        street: ['', Validators.required],
        city: ['',Validators.required],
        state: ['', Validators.required ],
        zip: ['', Validators.required ]
      }),
      sameAsBilling: [true],
      shippingAddress: this.formBuilder.group({  // TODO make this required when sameAsBilling becomes true
        street: [''],
        city: [''],
        state: ['' ],
        zip: ['' ]
      })
    });

    this.onChanges();
  }

  onChanges(){
    this.checkoutForm.statusChanges.subscribe(val => {
      this.canGoNext.emit(this.checkoutForm.valid);
    })
  }

}
