import { Component, 
  OnInit, 
  OnDestroy,
  Inject, 
  ViewChild, 
  ElementRef, 
  AfterViewInit, 
  ChangeDetectorRef } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { environment } from '../../../environments/environment';

declare var Stripe;
// declare var elements;

@Component({
  selector: 'app-payment-modal',
  templateUrl: './payment-modal.component.html',
  styleUrls: ['./payment-modal.component.css']
})
export class PaymentModalComponent implements OnInit, AfterViewInit, OnDestroy {

  key: string = environment.stripeKey;
  stripe = Stripe(this.key);
  elements = this.stripe.elements();

  nameOnCard: string = '';
  card: any;
  expiry: any;
  cvc: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  @ViewChild('cardInfo') cardInfo: ElementRef;
  @ViewChild('cardExpiry') cardExpiry: ElementRef;
  @ViewChild('cardCVC') cardCVC: ElementRef;


  constructor(
    public dialogRef: MatDialogRef<PaymentModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit(){

    const style = {
      base: {
        fontFamily: 'Raleway',
        color: 'white',
        fontWeight: 400,
        fontSmoothing: 'antialiased',
        fontSize: '19px',
        '::placeholder': {
          color: 'light-grey'
        }
      }
    };

    this.card = this.elements.create('cardNumber', {style});
    this.expiry = this.elements.create('cardExpiry', {style});
    this.cvc = this.elements.create('cardCvc', {style});

    this.card.mount(this.cardInfo.nativeElement);
    this.expiry.mount(this.cardExpiry.nativeElement);
    this.cvc.mount(this.cardCVC.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
    this.expiry.addEventListener('change', this.cardHandler);
    this.cvc.addEventListener('change', this.cardHandler);

  }

  ngOnDestroy(){
    this.card.removeEventListener('change', this.cardHandler);
    this.expiry.removeEventListener('change', this.cardHandler);
    this.cvc.removeEventListener('change', this.cardHandler);

    this.card.destroy();
    this.expiry.destroy();
    this.cvc.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async submit() {
    console.log(this.card, this.nameOnCard);
    const stripeResult = await this.stripe.createToken(this.card);
    if (stripeResult && stripeResult.error) {
      console.log('Something is wrong:', stripeResult.error);
      this.error = stripeResult.error.message;
    } else if(stripeResult && stripeResult.token) {
      console.log('Success!', stripeResult.token);
      this.dialogRef.close(stripeResult.token);
      // ...send the token to the your backend to process the charge
    }
  }
}
