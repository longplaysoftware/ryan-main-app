import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { ProductsService } from '../../services/products.service';
import { OrderService } from '../../services/order.service';
import { SingleProductOrder } from '../../interfaces/order';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})


export class ProductComponent implements OnInit {

  private orderForm: FormGroup;
  private availableProducts: any;

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private orderService: OrderService
  ) {}

  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      products: new FormArray([])
    });

    this.productsService.getAllSkus()
    .subscribe((skus) => {
      let products = this.transformProducts(skus);
      products.forEach(p => {
        (this.orderForm.get('products') as FormArray).push(this.createProductForm(p));
      })
      // console.log(skus);
      console.log(products);
    }, err => console.log(err));
    // this.productsService.getProducts()
    //   .subscribe(products => {
    //     products.forEach(p => {
    //       (this.orderForm.get('products') as FormArray).push(this.createProductForm(p));
    //     })
    //   });
  }

  getDigitalPrice(sku, p){
    if(!p['digitalPrice'] && sku['attributes']['licence_type'] === 'digital'){
      p['digitalPrice'] = sku['price']/100;
    }
  }

  getPhysicalPrice(sku, p){
    if(!p['physicalPrice'] && sku['attributes']['licence_type'] === 'physical'){
      p['physicalPrice'] = sku['price']/100;
    }
  }

  getVoicingOption(sku, p){
    let voicingOption = sku['attributes']['voicing_option'];
    if(!p['voicingOptions'].includes(voicingOption)) {
      p['voicingOptions'].push(voicingOption);
    }
  }

  transformProducts(skus){
    let accumulator = skus.data.reduce((acc, val) => {
      if(!acc.ids[val.product.id]){
        acc.ids[val.product.id] = val.product.id;
        let p = {};
        p['id'] = val.product.id;
        p['name'] = val.product.name;
        p['voicingOptions'] = [];
        p['skuInfo'] = {};
        skus.data.forEach((sku) => {
          if(sku['product']['id'] === val.product.id){
            this.getDigitalPrice(sku, p);
            this.getPhysicalPrice(sku, p);
            this.getVoicingOption(sku, p);
          }
        });
        p['url'] = '../../assets/product-images/'+this.getTitleCaseName(p['name']+' '+p['voicingOptions'][0])+'.png';
        console.log(p['url']);
        acc.results.push(p);
      }
      return acc;
    },{ids: {}, results: []});
    return accumulator['results'];
  }

  getTitleCaseName(name) {
    let retVal = name.split(' ').join('-');
    retVal = retVal.replace(/\./g, '');
    retVal = retVal.replace(/\?/g, '');
    retVal = retVal.replace(/\!/g, '');
    return retVal;
  }

  createProductForm(p) {
    let productForm = this.formBuilder.group({
      //read only properties
      name: [p.name, Validators.required],
      url: [p.url],
      digitalPrice: [p.digitalPrice],
      physicalPrice: [p.physicalPrice],
      voicingOptions: [p.voicingOptions],
      subTotal: [p.digitalPrice],

      options: new FormArray([])
    });

    (productForm.get('options') as FormArray).push(this.createOption(productForm));

    return productForm;
  }

  createOption(productForm: FormGroup): FormGroup {
    let p = productForm.value;

    let optionForm = this.formBuilder.group({
      //read only properties
      calculatedPrice: [p.digitalPrice],
      inCart: [false],

      //form properties
      voicingOption: [null, Validators.required],
      isPhysical: [false],
      numCopies: [1, [
        Validators.required,
        Validators.min(1),
        Validators.max(9999),
        Validators.maxLength(4)
      ]],
    });

    function getNewSubtotal() {
      let newTotal = 0;
      (productForm.get('options') as FormArray).controls.forEach((c) => {
        newTotal += c.get('calculatedPrice').value;
      })
      productForm.get('subTotal').setValue(newTotal, {onlySelf:true});
    }

    // Set up listeners to calculate product total when certain values change
    optionForm.get('isPhysical').valueChanges.subscribe((isPhysicalValue) => {
      let newPrice = isPhysicalValue ? (p.physicalPrice * optionForm.get('numCopies').value) : p.digitalPrice;
      optionForm.get('calculatedPrice').setValue(newPrice, {onlySelf:true});
      getNewSubtotal();
    });

    optionForm.get('numCopies').valueChanges.subscribe((numCopiesValue) => {
      let newPrice = p.physicalPrice * optionForm.get('numCopies').value;
      optionForm.get('calculatedPrice').setValue(newPrice, {onlySelf:true});
      getNewSubtotal();
    });

    optionForm.get('voicingOption').valueChanges.subscribe((voicingOptionValue) => {
      this.updateImage(productForm, voicingOptionValue);
      getNewSubtotal();
    });

    return optionForm;
  }

  updateImage(productForm, voicingOption) {
    let name = productForm.value.name;
    let url = '../../assets/product-images/'+this.getTitleCaseName(name+' '+voicingOption)+'.png';
    productForm.get('url').setValue(url)
    console.log(url);
  }

  removeOption(productIndex, optionIndex) {
    this.removeProduct(productIndex, optionIndex);
    this.getOptions(productIndex).removeAt(optionIndex);
  }

  isInCart(productIndex, optionIndex): boolean {
    return this.getOptions(productIndex).at(optionIndex).get('inCart').value;
  }

  getProducts(): FormArray {
    return this.orderForm.get('products') as FormArray;
  }

  getOptions(productIndex): FormArray {
    return this.getProducts().at(productIndex).get('options') as FormArray;
  }

  removeProduct(productIndex, optionIndex): void {
    this.getOptions(productIndex).at(optionIndex).get('inCart').setValue(false);
  }

  addProduct(productIndex, optionIndex): void {
    let thisProduct = this.getProducts().at(productIndex);
    let thisOption = this.getOptions(productIndex).at(optionIndex);
    thisOption.get('inCart').setValue(true);

    let thisProductValue = thisProduct.value;
    let thisOptionValue = thisOption.value;

    // this.orderForm.get('price').setValue(this.price);
    let productType = {
      name: thisProductValue.name,
      voicingOptions: thisProductValue.voicingOptions,
      physicalPrice: thisProductValue.physicalPrice,
      digitalPrice: thisProductValue.digitalPrice,
    }

    let submitValue: SingleProductOrder = {
      product: productType,
      voicingOption: thisOptionValue.voicingOption,
      price: thisOptionValue.calculatedPrice,
      isPhysical: thisOptionValue.isPhysical,
      numCopies: thisOptionValue.numCopies,
    };

    this.orderService.addProduct(submitValue);
  }

  addMore(orderIndex) {
    let productAtI = this.getProducts().at(orderIndex) as FormGroup;
    let productAtIOptions = productAtI.get('options') as FormArray;
    productAtIOptions.push(this.createOption(productAtI));
  }
}
