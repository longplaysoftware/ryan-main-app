import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { OrderService } from '../../services/order.service';
import { OrderType } from '../../interfaces/order';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {

  order: OrderType;
  orderSubscription: Subscription;

  @Output() canGoNext: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private orderService: OrderService
  ){}

  ngOnInit() {
    this.orderSubscription = this.orderService.order.subscribe( order => this.useOrder(order));
  }

  ngOnDestroy(){
    this.orderSubscription.unsubscribe();
  }

  useOrder(order){
    this.order = order;
    this.emitChange();
  }

  nextCondition(){
    return !!this.order.products.length
  }

  removeProduct(index){
    this.orderService.removeProduct(index);
    //TODO this needs to set the inCart flag to false
  }

  emitChange(){
    this.canGoNext.emit(this.nextCondition());
  }

}
