import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nav-buttons',
  templateUrl: './nav-buttons.component.html',
  styleUrls: ['./nav-buttons.component.css']
})
export class NavButtonsComponent implements OnInit {

  @Input()
  navParams: any;

  @Output()
  emitNav: EventEmitter<any> = new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  navTo(route: string){
    this.emitNav.emit(route);
  }

}
