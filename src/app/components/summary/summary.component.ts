import { Component, OnInit } from '@angular/core';
import { OrderType } from '../../interfaces/order';
import { OrderService } from '../../services/order.service';
import { PaymentsService } from '../../services/payments.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  order: OrderType;

  constructor(
    private orderService: OrderService,
    private paymentsService: PaymentsService
  ) { }

  ngOnInit() {
    this.orderService.order.subscribe(order => this.useOrder(order));
  }

  useOrder(order: OrderType){
    this.order = order;
    this.paymentsService.setOrder(order);
  }

}
