import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { NotificationsService } from '../services/notifications.service';

import { PaymentModalComponent } from '../components/payment-modal/payment-modal.component';
import { OrderType } from '../interfaces/order';

@Injectable()
export class PaymentsService {
  API = environment.apiBaseURL;
  order: OrderType;

  constructor(
    private dialog: MatDialog,
    private http: HttpClient,
    private router: Router,
    private notification: NotificationsService
  ) { }

  setOrder(order: OrderType){
    this.order = order;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PaymentModalComponent, {data: this.order});

    dialogRef.afterClosed().subscribe(token => {
      if(!token){
        return;
      }
      this.handlePayment(token);
    });
  }

  handlePayment(token){
    console.log('token: ',token);
    this.http.post(this.API + 'payments/charge', {token: token, order: this.order}).subscribe((outcome: any) => {
      this.handlePaymentOutcome(outcome);
    },
    error => console.log('error: ', error))
    // console.log(token);
    // make api call to be passing token and this.order;
    // be should try to bill the client using the stripe token and the order object
    // be on success the be will also save the order in the orders list
    // be on success the order state will be changed to charged
    // fe on success it should redirect to another page
    // fe on error it should bring up the message modal with the error
    // be on success an invoice will be creted, sent, and order state changed to billed
  }

  handlePaymentOutcome(outcome: any){
    if(outcome && outcome.network_status === 'approved_by_network'){
      this.router.navigate(['/payment-ok']);
    } else {
      const data = {message: 'There was a problem charging your card.'}
      this.notification.showMessage(data)
    }
  }

}
