import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

declare var Stripe;

@Injectable()
export class ProductsService {
  API: string = environment.apiBaseURL;
  stripeKey: string = environment.stripeKey;
  stripe = Stripe(this.stripeKey)

  constructor(private http: HttpClient) { }

  getProductsFromStripe(){
    return this.http.get(this.API + 'stripe-client/get-all-products');
  }

  getSkusByProduct(productId){
    return this.http.get(this.API + 'stripe-client/get-skus/' + productId);
  }

  getAllSkus(){
    return this.http.get(this.API + 'stripe-client/get-all-skus');
  }

  public getProducts = function () {
    return this.http.get('assets/mock-data/products.json');
  };
}
