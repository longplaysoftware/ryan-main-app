import { Injectable } from '@angular/core';
import { Order } from '../order';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class OrdersService {

  constructor(private http: HttpClient) { }

  public createOrder = function (orderData) {
    console.log('createOrder', orderData);
    return this.http.post(environment.apiBaseURL + 'orders', orderData).subscribe(
      data => {
        console.log("success", data);
      },
      err => {
        console.log("error", err);
      }
    )
  };
}
