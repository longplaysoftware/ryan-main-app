import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { OrderType, SingleProductOrder, OrderState } from '../interfaces/order';
import { Account, Address } from '../interfaces/account';


@Injectable()
export class OrderService {

  orderState: any = {
    products: [],
    account: {},
    billingAddress: {},
    shippingAddress: {},
    cost: {
      subTotal: 0,
      shipping: 0,
      total: 0
    },
    state: OrderState.CREATED
  }

  private orderBSubject = new BehaviorSubject<any>(this.orderState);
  public order = this.orderBSubject.asObservable();

  constructor() { }

  emitNewState(newOrderState: OrderType){
    newOrderState = this.calculateFields(newOrderState);
    this.orderBSubject.next(newOrderState);
  }

  calculateFields(order: OrderType): OrderType{      
    order.cost.subTotal = this.calcSubTotal(order);
    order.cost.shipping = this.calcShippingCost(order);    
    order.cost.total = order.cost.shipping + order.cost.subTotal;
    return order;
  }

  calcSubTotal(order: OrderType): number {    
    return order.products.reduce((acc, val) => {
      if(val){
        acc+= val.price;
      }
      return acc;
    }, 0);
  }

  // this needs to be changed with an algorythm
  // that actually calculates shipping costs on the basis of the distance
  calcShippingCost(order: OrderType): number{
    return order.products.reduce((acc, val) => {
      if(val && val.isPhysical){
        acc+= val.price;
      }
      return acc;
    }, 0);
  }

  thereArePhysicalProducts(order: OrderType){
    let res: boolean = false;
    if(order.products && order.products.length){
      res = order.products.reduce((acc, val) =>{
        if(val.isPhysical){
          acc = true;
        }
        return acc;
      }, res)
    }
    return res;
  }

  addProduct(product: SingleProductOrder){    
    this.orderState.products.push(product);
    this.emitNewState(this.orderState);    
  }

  removeProduct(index: number){
    this.orderState.products.splice(index, 1);
    this.emitNewState(this.orderState);
  }

  addAccount(account: Account){
    this.orderState['account'] = account;
    this.emitNewState(this.orderState);
  }

  removeAccount(){
    delete this.orderState.account;
    this.emitNewState(this.orderState);
  }

  addBillingAddress(address: Address){
    this.orderState['billingAddress'] = address;
    this.emitNewState(this.orderState);
  }

  removeBillingAddress(){
    delete this.orderState.billingAddress;
    this.emitNewState(this.orderState);
  }

  addShippingAddress(address: Address){
    this.orderState['shippingAddress'] = address;
    this.emitNewState(this.orderState);
  }

  removeShippingAddress(){
    delete this.orderState.shippingAddress;
    this.emitNewState(this.orderState);
  }

  addressValid(){
    if(this.thereArePhysicalProducts(this.orderState)){
      return !!this.orderState.billingAddress && !!this.orderState.shippingAddress
    } else {
      return !!this.orderState.billingAddress
    }
  }

  

}
