import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';

import { NotificationComponent } from '../components/notification/notification.component';


@Injectable()
export class NotificationsService {

  constructor(private dialog: MatDialog) { }

  showMessage(data): void {
    this.dialog.open(NotificationComponent, {data: data});
  }

}
