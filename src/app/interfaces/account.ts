export interface Account {
  accountName: string;
  accountNumber: string;
  firstName: string;
  lastName: string;
  confirmed?: boolean; //this is an helper field to check if the user is editing info on the form
}

export interface Address {
  street: string;
  city: string;
  state: string;
  zip: string;
}