import { ProductType, VoicingOptionEnum } from './product';
import { Account, Address } from './account';

export interface OrderType {
  _id?: string;
  account?: Account;
  billingAddress?: Address;
  shippingAddress?: Address;
  products?: SingleProductOrder[];
  cost?: orderCost;
  state?: OrderState
}

export interface orderCost {
  subTotal?: number;
  shipping?: number;
  total: number;
}

export interface SingleProductOrder {
  product?: ProductType;
  voicingOption?: VoicingOptionEnum;
  price?: number;
  isPhysical: boolean;
  numCopies?: number;
}

export enum OrderState {
  CREATED = 'created',
  CHARGED = 'charged',
  BILLED = 'billed',
  SHIPPED = 'shipped',
  FULLFILLED = 'fullfilled'
}

/* The data might look like this eventually
{
    "_id" : ObjectId("5a958c2b0afc3b0fa4a92cf3"),
    "customerId" : null,
    "products" : [
        {
          productId: ObjectId("5a958c2b0afc3b0fa4a92cec"),
          isPhysical: true,
          numCopies: 50,
          voicingOption: 'TTB'
        },
        {
          productId: ObjectId("5a958c2b0afc3b0fa4a92cef"),
          isPhysical: false,
          voicingOption: 'SATB'
        }
    ],
    "subTotal" : 180,
    "shipping" : 3,
    "total" : 183
}
*/