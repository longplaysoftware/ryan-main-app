export enum VoicingOptionEnum {
  SATB = "SATB",
  TTB = "TTB",
  SAB = "SAB",
  SSA = "SSA",
  SSAB = "SSAB"
}

export interface ProductType {
  _id?: string;
  name: string;
  voicingOptions: VoicingOptionEnum[];
  physicalPrice: number;
  digitalPrice: number;
}

