export class Product {
  id = '';
  orderDetails = new OrderDetails();
}

export class OrderDetails {
  name = '';
  voicingOptions = [];
  physicalPrice = 0;
  digitalPrice = 0;

  isPhysical = false;
  copies = 1;
  voicingOption = '';
  total = 0;

  constructor(options: any = {}) {
    this.voicingOptions = options.voicingOptions || [];
    this.physicalPrice = options.physicalPrice || 0;
    this.digitalPrice = options.digitalPrice || 0;

    this.isPhysical = options.isPhysical || false;
    this.copies = options.copies || 1;
    this.voicingOption = options.voicingOption || '';
    this.total = options.total || 0;
  }
}
