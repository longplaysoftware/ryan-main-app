import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentOkComponent } from './payment-ok.component';

describe('PaymentOkComponent', () => {
  let component: PaymentOkComponent;
  let fixture: ComponentFixture<PaymentOkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentOkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentOkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
