import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from '../../services/order.service';
import { PaymentsService } from '../../services/payments.service';
import { Subscription } from 'rxjs/Subscription';

import { OrderType } from '../../interfaces/order';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit, OnDestroy {

  order: OrderType;
  orderSubscription: Subscription;
  currentView: string = 'products';
  nextEnabled: boolean;
  navParams: any = {};

  constructor(
    private orderService: OrderService,
    private paymentsService: PaymentsService,
    private router: Router
  ){}

  ngOnInit() {
    this.orderSubscription = this.orderService.order.subscribe( order => this.useOrder(order));
  }

  ngOnDestroy(){
    this.orderSubscription.unsubscribe();
  }

  useOrder(order: OrderType){
    this.order = order;
    this.setNavParams();
  }

  setNavParams(){
    this.navParams['save'] = null;
    this.navParams['back'] = null;
    switch(this.currentView){
      case 'products':
        this.navParams['next'] = {
          route: 'checkout',
          display: 'Proceed to checkout',
          enabled: this.nextEnabled
        };
        break;
      case 'checkout':
        this.navParams['back'] = 'products';
        this.navParams['backDisplay'] = 'Back to Products';
        this.navParams['next'] = {
          route: 'summary',
          display: 'Review your order',
          enabled: this.nextEnabled
        };
        break;
      case 'summary':
        this.navParams['back'] = 'checkout';
        this.navParams['backDisplay'] = 'Back to Checkout'
        this.navParams['next'] = {
          route: 'payment',
          display: 'Pay',
          enabled: true
        };
        this.navParams['save'] = {
          route: 'save',
          display: 'Place order',
          enabled: true
        }
      break;
    }

  }

  navTo(view: string){
    console.log(view);
    if(view === 'save'){
      this.saveOrder();
    }
    else if(view === 'payment'){
      this.paymentsService.openDialog();
    } else {
      this.currentView = view;
      this.setNavParams();
    }
  }

  toggleNext(canGoNext: boolean){
    this.nextEnabled = canGoNext;
    this.setNavParams();
  }

  saveOrder(){

  }

}
