import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormGroupDirective, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';


import {
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatCheckboxModule,
  MatSlideToggleModule,
  MatListModule,
  MatIconModule,
  MatExpansionModule,
  MatDialogModule
} from '@angular/material';

import { UsStatesService } from './services/us-states.service';
import { ProductsService } from './services/products.service';
import { OrdersService } from './services/orders.service';
import { OrderService } from './services/order.service';
import { PaymentsService } from './services/payments.service';
import { NotificationsService } from './services/notifications.service';

import { AppComponent } from './app.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { ProductComponent } from './components/product/product.component';
import { ProductsComponent } from './components/products/products.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { NavButtonsComponent } from './components/nav-buttons/nav-buttons.component';
import { SummaryComponent } from './components/summary/summary.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { PaymentModalComponent } from './components/payment-modal/payment-modal.component';
import { NotificationComponent } from './components/notification/notification.component';
import { PaymentOkComponent } from './pages/payment-ok/payment-ok.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'orders',
    pathMatch: 'full'
  },
  {
    path: 'orders',
    component: OrdersComponent
  },
  {
    path: 'payment-ok',
    component: PaymentOkComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    OrdersComponent,
    ProductsComponent,
    CheckoutComponent,
    NavButtonsComponent,
    SummaryComponent,
    PaymentComponent,
    PaymentModalComponent,
    NotificationComponent,
    PaymentOkComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatListModule,
    MatIconModule,
    MatExpansionModule,
    MatDialogModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    UsStatesService,
    ProductsService,
    OrdersService,
    OrderService,
    PaymentsService,
    NotificationsService,
    FormGroupDirective
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    PaymentModalComponent,
    NotificationComponent
  ]
})
export class AppModule { }
