import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsStatesService } from './services/us-states.service';
import { OrdersService } from './services/orders.service';
import { Product, OrderDetails } from './product';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  versionNumber: string;

  constructor(
  )
  {
    this.versionNumber = environment.version;
  }

  ngOnInit() {
  }

  
}
