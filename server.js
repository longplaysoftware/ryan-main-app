const express = require('express');
const app = express();
const path = require('path');

const letsEncryptResponse = process.env.CERTBOT_RESPONSE;

const forceSSL = function() {
  return function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(
       ['https://', req.get('Host'), req.url].join('')
      );
    }
    next();
  }
}

// Redirect http to https in prod
if(process.env.NODE_ENV === 'production') {
  app.use(forceSSL());
}

// Return the Let's Encrypt certbot response:
app.get('/.well-known/acme-challenge/:content', function(req, res) {
  res.send(letsEncryptResponse);
});

// Serve the static files from the dist directory
app.use(express.static(__dirname + '/dist'));

// For all GET requests, send back index.html so that PathLocationStrategy can be used
app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

// Start the app by listening on the established port or 4200
app.listen(process.env.PORT || 3000);
